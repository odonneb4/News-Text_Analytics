import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import string
# download necessary NLTK data
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
#import spacy
#nlp = spacy.load('en_core_web_sm')

import re
import pandas as pd
import os
# define the sentence to be processed
import csv
count = 0
def remove_punctuations(content,PUNCTUATIONS):
    return str(content).translate(str.maketrans('', '', PUNCTUATIONS))
def remove_URLS(content):
    return re.sub('((www.\S+)|(http[s]?://\S+))',' ', str(content))

def process_sentence(sentence):
    sentence = re.sub(r'[^\w\s]', '', sentence)
    PUNCTUATIONS = string.punctuation
    sentence = remove_URLS(sentence)
    sentence = remove_punctuations(sentence,PUNCTUATIONS)
    # convert the sentence to lowercase
    sentence = sentence.lower()

    # tokenize the sentence into individual words
    #doc =nlp('I got a red candy and an interesting and big book.')
    words = word_tokenize(sentence)

    # remove stop words from the list of words
    stop_words = set(stopwords.words('english'))
    words = [word for word in words if word not in stop_words ]

    # lemmatize the words using WordNetLemmatizer
    lemmatizer = WordNetLemmatizer()
    words = [lemmatizer.lemmatize(word) for word in words ]
    if len(words) == 0:
        words=['NULL']
    return words


path = "sample_csv"
filenames = os.listdir(path)
for file_name in filenames:
    with open('sample_csv_preprocessed/'+file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        csv_file = pd.read_csv('sample_csv/'+file_name,skipinitialspace=True,usecols=['Paragraphs','Captions'])
        for index, row in csv_file.iterrows():
            
            count = count+1
            list=[]
            content = row['Paragraphs']
            caption = row['Captions']
            # do something with the value of the column, such as printing it
            if(count%1000 == 0):
                print(file_name+" : "+str(count))
            try:
                processed_content = process_sentence(content)
            except:
                continue
                processed_content = ["NULL"]
            try:
                processed_caption = process_sentence(caption)
            except:
                continue
                processed_caption = ["NULL"]
            writer.writerow([processed_content,processed_caption])

# print the list of lemmatized and stop-word-free words



