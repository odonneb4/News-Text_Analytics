import nltk
from nltk.corpus import wordnet
import csv
import pandas as pd

# with open('wordNet.csv', 'w', newline='') as file:
#     writer = csv.writer(file)
#     # Load the lemmantized CSV file
#     df = pd.read_csv('processed.csv', header=None, names=['content', 'caption'])
#     for index, row in df.iterrows():
#         # Create new columns for the synsets
#         df['caption_senses'] = None
#         df['content_senses'] = None
#         # Use boolean indexing to select rows that do not contain ['NULL']
#         df = df[~df.apply(lambda row: any(val == "['NULL']" for val in row.values), axis=1)]

#         for index, row in df.iterrows():
#             df['caption_senses'] = df['caption'].apply(lambda x: [wordnet.synsets(word) for word in x])
#             df['content_senses'] = df['content'].apply(lambda x: [wordnet.synsets(word) for word in x])
#         writer.writerow(df)
#     print(df.info())

def get_word_senses(source_csv, output_csv):
    # Load the lemmantized CSV file
    df = pd.read_csv(source_csv, header=None, names=['content', 'caption'])
    for index, row in df.iterrows():
        # Create new columns for the synsets
        df['caption_senses'] = None
        df['content_senses'] = None
        # Use boolean indexing to select rows that do not contain ['NULL']
        df = df[~df.apply(lambda row: any(val == "['NULL']" for val in row.values), axis=1)]

        for index, row in df.iterrows():
            df['caption_senses'] = df['caption'].apply(lambda x: [wordnet.synsets(word) for word in x])
            df['content_senses'] = df['content'].apply(lambda x: [wordnet.synsets(word) for word in x])
    # writer.writerow(df)
    df.to_csv(output_csv, header= True, index=False)
    print(df.info())
