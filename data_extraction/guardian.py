# import pandas as pd
# import json
# import os
# from bs4 import BeautifulSoup

# # create an empty list to store the data
# data = []

# # set the directory where the JSON files are located
# directory = 'C:/Users/BOD/Downloads/articles/articles/guardian/guardian_json/'

# # iterate over all the files in the directory
# for filename in os.listdir(directory):
#     if filename.endswith('.json'):
#         file_path = os.path.join(directory, filename)
#         try:
#             # load the JSON object from the file
#             with open(file_path, 'r', encoding='utf-8') as f:
#                 json_object = json.load(f)
#             # extract the body and caption fields from the JSON object
            
#             # add the data to the list
#             data.append({'filename': filename,'body': body_text, 'caption': caption, 'section': section})
#             if len(data) >= 1000:
#                 break        
#         except:
#             print(f"Error occurred while processing {filename}. Skipping...")

# # create a dataframe from the list of data
# df = pd.DataFrame(data)

# # print the first 5 rows of the dataframe
# print(df.head())
# # print the size of the dataframe
# print(f"DataFrame size: {df.shape}")

# df.to_csv('guardian_data.csv', index=False)


import json
import os
import pandas as pd
import re
from urllib.parse import urlparse
from bs4 import BeautifulSoup

# Set directory path
directory_path = "C:/Users/BOD/Downloads/articles/articles/guardian/guardian_json"

def remove_non_ascii(text):
    return ''.join(char for char in text if ord(char) < 128)

def remove_html_tags(text):
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

def load_json(file_path):
    with open(file_path) as f:
        try:
            return json.load(f)
        except json.JSONDecodeError:
            with open(file_path, 'r') as f:
                data = f.read()
                # Fix the JSON data here
                fixed_data = data.replace("'", "\"")
                return json.loads(fixed_data)

# Count the number of JSON files in the directory
json_count = len([filename for filename in os.listdir(directory_path) if filename.endswith(".json")])

# Initialize variables for tracking progress
json_processed_count = 0
json_success_count = 0
json_failure_count = 0

# Initialize dataframe
df = pd.DataFrame(columns=["Filename","Section", "Paragraphs", "Captions"])

# Iterate through each JSON file in the directory
for filename in os.listdir(directory_path):
    if filename.endswith(".json"):
        try:
            # Load the JSON data from file
            data = load_json(os.path.join(directory_path, filename))

            # extract the section field
            section = data['section']

            paragraphs = remove_non_ascii(remove_html_tags(data["body"]))
            body = paragraphs.replace('\n', ' ')
            body = body.replace("\'", '')
            body = body.replace('\"', '')
            
            captions = []
            # Extract the captions from the JSON data
            if 'displayImages' in data:
                for image in data['displayImages']:
                    if 'cleanCaption' in image:
                        captions.append(remove_non_ascii(remove_html_tags(image['cleanCaption'])))
            if 'bodyImages' in data:
                for image in data['bodyImages']:
                    if 'cleanCaption' in image:
                        captions.append(remove_non_ascii(remove_html_tags(image['cleanCaption'])))
            if 'headerImages' in data:
                for image in data['headerImages']:
                    if 'cleanCaption' in image:
                        captions.append(remove_non_ascii(remove_html_tags(image['cleanCaption'])))

            # Append to dataframe
            df = df.append({"Filename": filename,"Section": section, "Paragraphs": body, "Captions": " ".join(captions)}, ignore_index=True)

            # Update success count
            json_success_count += 1

        except:
            print(f"Error occurred while processing {filename}.")
            # Update failure count
            json_failure_count += 1

        # Update processed count and print progress
        json_processed_count += 1
        print(f"{json_processed_count}/{json_count} JSON files processed. {json_success_count} saved successfully. {json_failure_count} failed.")

        # if (json_success_count>1000):
        #     break

# Save dataframe as CSV
df.to_csv("guardian.csv", index=False)

print(f"All {json_processed_count} JSON files processed. {json_success_count} saved successfully. {json_failure_count} failed.")
