import json
import os
import pandas as pd
import re

def remove_non_ascii(text):
    return ''.join(char for char in text if ord(char) < 128)

def remove_html_tags(text):
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

def load_json(file_path):
    with open(file_path) as f:
        try:
            return json.load(f)
        except json.JSONDecodeError:
            with open(file_path, 'r') as f:
                data = f.read()
                # Fix the JSON data here
                fixed_data = data.replace("'", "\"")
                return json.loads(fixed_data)

# Set directory path
directory_path = "C:/Users/BOD/Downloads/articles/articles/washington_post/washington_post_articles/json"

# Count the number of JSON files in the directory
json_count = len([filename for filename in os.listdir(directory_path) if filename.endswith(".json")])

# Initialize variables for tracking progress
json_processed_count = 0
json_success_count = 0
json_failure_count = 0

# Initialize dataframe
df = pd.DataFrame(columns=["Filename","Section", "Paragraphs", "Captions"])

# Iterate through each JSON file in the directory
for filename in os.listdir(directory_path):
    if filename.endswith(".json"):
        try:
            # Load the JSON data from file
            data = load_json(os.path.join(directory_path, filename))

            section = data["sourcesection"]

            # Extract the text paragraphs from the JSON data
            paragraphs = [remove_non_ascii(remove_html_tags(p['content'])) for p in data['items'] if p['type'] == "sanitized_html"]
            
            # Extract the captions from the JSON data
            captions = [remove_non_ascii(remove_html_tags(p['fullcaption'])) for p in data['items'] if p['type'] == "image"]

            # Append to dataframe
            df = df.append({"Filename":filename,"Section": section, "Paragraphs": " ".join(paragraphs), "Captions": " ".join(captions)}, ignore_index=True)

            # Update success count
            json_success_count += 1

        except:
            print(f"Error occurred while processing {filename}.")
            # Update failure count
            json_failure_count += 1

        # Update processed count and print progress
        json_processed_count += 1
        print(f"{json_processed_count}/{json_count} JSON files processed. {json_success_count} saved successfully. {json_failure_count} failed.")

        # if (json_success_count>1000):
        #     break

# Save dataframe as CSV
df.to_csv("washington_post.csv", index=False)

print(f"All {json_processed_count} JSON files processed. {json_success_count} saved successfully. {json_failure_count} failed.")
