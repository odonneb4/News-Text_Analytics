import csv
import random
import pandas as pd

# specify the path to your CSV file
bbc_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/bbc.csv"
usatoday_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/usa_today.csv"
guardian_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/guardian.csv"
washington_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/washington_post.csv"

# read in the input CSV file as a pandas dataframe
bbc_df = pd.read_csv(bbc_file)
bbc_rows = bbc_df.sample(n=20000, random_state=1)
usatoday_df = pd.read_csv(usatoday_file)
usatoday_rows = usatoday_df.sample(n=20000, random_state=1)
guardian_df = pd.read_csv(guardian_file)
guardian_rows = guardian_df.sample(n=20000, random_state=1)
washington_df = pd.read_csv(washington_file)
washington_rows = washington_df.sample(n=20000, random_state=1)


bbc_rows['Captions'] = bbc_rows['Captions'].str.strip()
bbc_rows['Paragraphs'] = bbc_rows['Paragraphs'].str.strip()
bbc_df_sample = bbc_rows.dropna(subset=['Captions'], thresh=1)
bbc_df_sample = bbc_df_sample.dropna(subset=['Paragraphs'], thresh=1)
bbc_df_sample = bbc_df_sample[bbc_df_sample['Captions'] != ""]
bbc_df_sample = bbc_df_sample[bbc_df_sample['Paragraphs'] != ""]

usatoday_df_sample = usatoday_rows[usatoday_rows['Captions'] != "[]"]
usatoday_df_sample['Paragraphs'] = usatoday_df_sample['Paragraphs'].str.strip()
usatoday_df_sample = usatoday_df_sample[usatoday_df_sample['Paragraphs'] != ""]
usatoday_df_sample = usatoday_df_sample.dropna(subset=['Paragraphs'], thresh=1)

guardian_rows['Captions'] = guardian_rows['Captions'].str.strip()
guardian_rows['Paragraphs'] = guardian_rows['Paragraphs'].str.strip()
guardian_df_sample = guardian_rows.dropna(subset=['Captions'], thresh=1)
guardian_df_sample = guardian_df_sample.dropna(subset=['Paragraphs'], thresh=1)
guardian_df_sample = guardian_df_sample[guardian_df_sample['Captions'] != ""]
guardian_df_sample = guardian_df_sample[guardian_df_sample['Paragraphs'] != ""]

washington_rows['Captions'] = washington_rows['Captions'].str.strip()
washington_rows['Paragraphs'] = washington_rows['Paragraphs'].str.strip()
washington_df_sample = washington_rows.dropna(subset=['Captions'], thresh=1)
washington_df_sample = washington_df_sample.dropna(subset=['Paragraphs'], thresh=1)
washington_df_sample = washington_df_sample[washington_df_sample['Captions'] != ""]
washington_df_sample = washington_df_sample[washington_df_sample['Paragraphs'] != ""]

bbc_csv = bbc_df_sample.sample(n=10000, random_state=1)
usatoday_csv = usatoday_df_sample.sample(n=10000, random_state=1)
guardian_csv = guardian_df_sample.sample(n=10000, random_state=1)
washington_csv = washington_df_sample.sample(n=10000, random_state=1)

usatoday_csv = usatoday_csv.drop(columns=['Subsection'])

bbc_csv.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/bbc_sample.csv", index=False)
usatoday_csv.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/usatoday_sample.csv", index=False)
guardian_csv.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/guardian_sample.csv", index=False)
washington_csv.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/washington_sample.csv", index=False)


washington_lifestyle_rows = washington_df[washington_df['Section'] == 'lifestyle']
guardian_lifestyle_rows = guardian_df[guardian_df['Section'] == 'lifestyle']
usatoday_lifestyle_rows = usatoday_df[usatoday_df['Section'] == 'life']
lifestyle_df = pd.concat([usatoday_lifestyle_rows, guardian_lifestyle_rows, washington_lifestyle_rows], axis=0)
lifestyle_df['Captions'] = lifestyle_df['Captions'].str.strip()
lifestyle_df['Paragraphs'] = lifestyle_df['Paragraphs'].str.strip()
lifestyle_df = lifestyle_df.dropna(subset=['Captions'], thresh=1)
lifestyle_df = lifestyle_df[lifestyle_df['Captions'] != "[]"]
lifestyle_df = lifestyle_df[lifestyle_df['Captions'] != ""]
lifestyle_df = lifestyle_df.dropna(subset=['Paragraphs'], thresh=1)
lifestyle_df = lifestyle_df[lifestyle_df['Paragraphs'] != ""]
lifestyle_df.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/lifestyle_sample.csv", index=False)

washington_opinion_rows = washington_df[washington_df['Section'] == 'opinion']
guardian_opinion_rows = guardian_df[guardian_df['Section'] == 'opinion']
usatoday_opinion_rows = usatoday_df[usatoday_df['Section'] == 'opinion']
opinion_df = pd.concat([usatoday_opinion_rows, guardian_opinion_rows, washington_opinion_rows], axis=0)
opinion_df['Captions'] = opinion_df['Captions'].str.strip()
opinion_df['Paragraphs'] = opinion_df['Paragraphs'].str.strip()
opinion_df = opinion_df.dropna(subset=['Captions'], thresh=1)
opinion_df = opinion_df[opinion_df['Captions'] != "[]"]
opinion_df = opinion_df[opinion_df['Captions'] != ""]
opinion_df = opinion_df.dropna(subset=['Paragraphs'], thresh=1)
opinion_df = opinion_df[opinion_df['Paragraphs'] != ""]
opinion_df.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/opinion_sample.csv", index=False)

washington_world_rows = washington_df[washington_df['Section'] == 'world']
guardian_world_rows = guardian_df[guardian_df['Section'] == 'world']
usatoday_world_rows = usatoday_df[usatoday_df['Subsection'] == 'world']
world_df = pd.concat([usatoday_world_rows, guardian_world_rows, washington_world_rows], axis=0)
world_df['Captions'] = world_df['Captions'].str.strip()
world_df['Paragraphs'] = world_df['Paragraphs'].str.strip()
world_df = world_df.dropna(subset=['Captions'], thresh=1)
world_df = world_df[world_df['Captions'] != "[]"]
world_df = world_df[world_df['Captions'] != ""]
world_df = world_df.dropna(subset=['Paragraphs'], thresh=1)
world_df = world_df[world_df['Paragraphs'] != ""]
world_df.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/world_sample.csv", index=False)

washington_politics_rows = washington_df[washington_df['Section'] == 'politics']
guardian_politics_rows = guardian_df[guardian_df['Section'] == 'politics']
usatoday_politics_rows = usatoday_df[usatoday_df['Subsection'] == 'politics']
politics_df = pd.concat([usatoday_politics_rows, guardian_politics_rows, washington_politics_rows], axis=0)
politics_df['Captions'] = politics_df['Captions'].str.strip()
politics_df['Paragraphs'] = politics_df['Paragraphs'].str.strip()
politics_df = politics_df.dropna(subset=['Captions'], thresh=1)
politics_df = politics_df[politics_df['Captions'] != "[]"]
politics_df = politics_df[politics_df['Captions'] != ""]
politics_df = politics_df.dropna(subset=['Paragraphs'], thresh=1)
politics_df = politics_df[politics_df['Paragraphs'] != ""]
politics_df.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/politics_sample.csv", index=False)

washington_sport_rows = washington_df[washington_df['Section'] == 'sport']
guardian_sport_rows = guardian_df[guardian_df['Section'] == 'sport']
usatoday_sport_rows = usatoday_df[usatoday_df['Section'] == 'sport']
sport_df = pd.concat([usatoday_sport_rows, guardian_sport_rows, washington_sport_rows], axis=0)
sport_df['Captions'] = sport_df['Captions'].str.strip()
sport_df['Paragraphs'] = sport_df['Paragraphs'].str.strip()
sport_df = sport_df.dropna(subset=['Captions'], thresh=1)
sport_df = sport_df.dropna(subset=['Paragraphs'], thresh=1)
sport_df = sport_df[sport_df['Captions'] != "[]"]
sport_df = sport_df[sport_df['Captions'] != ""]
sport_df = sport_df[sport_df['Paragraphs'] != ""]
sport_df.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/News-Text_Analytics/sample_csv/sport_sample.csv", index=False)