import csv
import random
import pandas as pd

# specify the path to your CSV file
bbc_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/bbc.csv"
usatoday_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/usa_today.csv"
guardian_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/guardian.csv"
washington_file = "C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/washington_post.csv"

# read in the input CSV file as a pandas dataframe
bbc_df = pd.read_csv(bbc_file)

# randomly select 25 rows from the dataframe (excluding the header row)
bbc_rows = bbc_df.sample(n=35, random_state=1)

# save the selected rows to a new CSV file
bbc_rows.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/bbc_sample.csv", index=False)

# read in the input CSV file as a pandas dataframe
usatoday_df = pd.read_csv(usatoday_file)

# randomly select 25 rows from the dataframe (excluding the header row)
usatoday_rows = usatoday_df.sample(n=35, random_state=1)

# save the selected rows to a new CSV file
usatoday_rows.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/usatoday_sample.csv", index=False)

# read in the input CSV file as a pandas dataframe
guardian_df = pd.read_csv(guardian_file)

# randomly select 25 rows from the dataframe (excluding the header row)
guardian_rows = guardian_df.sample(n=35, random_state=1)

# save the selected rows to a new CSV file
guardian_rows.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/guardian_sample.csv", index=False)

# read in the input CSV file as a pandas dataframe
washington_df = pd.read_csv(washington_file)

# randomly select 25 rows from the dataframe (excluding the header row)
washington_rows = washington_df.sample(n=35, random_state=1)

# save the selected rows to a new CSV file
washington_rows.to_csv("C:/myProjects/MSc Programming/CS7IS4_TextAnalytics/VisualNews-Repository/washington_sample.csv", index=False)